function love.load()
	socket = require("socket")
	hostUDP()
	love.graphics.setDefaultFilter("nearest")
	doggy = love.graphics.newImage("doggy.png")
	host_p = {x=90,y=70,look=1}
	client_p = {x=90,y=70,look=1}
	end

function love.update(dt)
	if love.keyboard.isDown("right") then host_p.x = math.floor(host_p.x + 550 * dt) ; host_p.look = -1 ; sendUDP() end
	if love.keyboard.isDown("left") then host_p.x = math.floor(host_p.x - 200 * dt) ; host_p.look = 1 ; sendUDP() end
	if love.keyboard.isDown("down") then host_p.y = math.floor(host_p.y + 550 * dt) ; sendUDP() end
	if love.keyboard.isDown("up") then host_p.y = math.floor(host_p.y - 200 * dt) ; sendUDP() end
	updateUDP()
	end

function love.draw(dt)
	love.graphics.setColor(0.5,0.7,1,1)
	love.graphics.draw(doggy, host_p.x - host_p.look * 6 * 17, host_p.y, 0, host_p.look*6, 6)
	love.graphics.setColor(1,0,0.6,1)
	love.graphics.draw(doggy, client_p.x - client_p.look * 6 * 17, client_p.y, 0, client_p.look*6, 6)
	end

function love.keypressed(key,scancode,isrepeat)
	if key == "escape" then love.event.quit() end
	end
	
function hostUDP()
	server = assert(socket.udp())
	print("open ip ('nil' for localhost): ")
	local get_ip = io.read(); if get_ip == 'nil' then own_ip = '127.0.0.1' else own_ip = get_ip end
	print("open port: ")
	own_port = io.read()
	print("listen as server? (y/n)")
	local answer = io.read()
	if answer ~= 'y' then 
	print("peer ip: ('nil' for localhost): ")
	local get_ip2 = io.read(); if get_ip2 == 'nil' then peer_ip = '127.0.0.1' else peer_ip = get_ip2 end
	print("peer port: ")
	peer_port = io.read() end
	server:setsockname(own_ip, own_port)
	ip, port = server:getsockname()
	print( "listening in " .. own_ip .. " on port " .. own_port .. "..." )
	if peer_ip then print( "connecting to " .. peer_ip .. " on port " .. peer_port .. "..." ) end
	server:settimeout(0.01)
	end

function sendUDP()
	if peer_ip then server:sendto( host_p.x .. ":" .. host_p.y .. ":" .. host_p.look, peer_ip, peer_port) end
	end

function updateUDP()
	local datagram, ip, port = server:receivefrom()
	if datagram then
		for x, y, look in datagram:gmatch("(.-):(.-):(.*)") do
			client_p.x = x
			client_p.y = y
			client_p.look = look
			end
		peer_ip = ip; peer_port = port
		print(datagram)
		end
	end